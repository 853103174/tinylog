package org.apache.commons.logging;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public abstract class LogFactory {

	private static final ConcurrentMap<String, Log> loggerMap = new ConcurrentHashMap<String, Log>();

	private LogFactory() {
	}

	public static Log getLog(Class<?> clazz) {
		return getLog(clazz.getName());
	}

	public static Log getLog(String name) {
		Log commonsLogger = loggerMap.get(name);
		if (commonsLogger == null) {
			Log newInstance = new CommonsLogging(name);
			Log oldInstance = loggerMap.putIfAbsent(name, newInstance);
			return oldInstance == null ? newInstance : oldInstance;
		} else {
			return commonsLogger;
		}
	}

}