package org.slf4j.impl;

import org.apache.commons.logging.Log;
import org.slf4j.helpers.MarkerIgnoringBase;

import log.tiny.TinyLog;

public final class JCLLoggerAdapter extends MarkerIgnoringBase {

	private static final long serialVersionUID = 4141593417490482209L;
	private static final TinyLog log = TinyLog.getInstance();
	private String className;

	JCLLoggerAdapter(Log logs, String name) {
		this.className = name;
	}

	private StringBuilder format(String format, Object... args) {
		char[] chars = format.toCharArray();
		int length = chars.length, a = -1, size = args.length - 1;
		StringBuilder infos = new StringBuilder(format.length() << 2);
		for (int i = 0; i < length; i++) {
			if (chars[i] == '{') {
				if (chars[i + 1] == '}') {
					if (a < size) {
						infos.append(args[++a]);
					} else {
						infos.append(args[a]);
					}
					++i;
					continue;
				}
			}

			infos.append(chars[i]);
		}

		return infos;
	}

	@Override
	public boolean isTraceEnabled() {
		return log.isTraceEnabled();
	}

	@Override
	public void trace(String msg) {
		if (log.denyClassName(className))
			return;
		log.trace(className);
		log.trace(msg);
	}

	@Override
	public void trace(String format, Object arg) {
		if (log.denyClassName(className))
			return;
		log.trace(className);
		log.trace(format(format, arg));
	}

	@Override
	public void trace(String format, Object arg1, Object arg2) {
		if (log.denyClassName(className))
			return;
		log.trace(className);
		log.trace(format(format, arg1, arg2));
	}

	@Override
	public void trace(String format, Object... arguments) {
		if (log.denyClassName(className))
			return;
		log.trace(className);
		log.trace(format(format, arguments));
	}

	@Override
	public void trace(String format, Throwable throwable) {
		if (log.denyClassName(className))
			return;
		log.trace(className);
		log.trace(null, throwable);
	}

	@Override
	public boolean isDebugEnabled() {
		return log.isDebugEnabled();
	}

	@Override
	public void debug(String msg) {
		if (log.denyClassName(className))
			return;
		log.debug(className);
		log.debug(msg);
	}

	@Override
	public void debug(String format, Object arg) {
		if (log.denyClassName(className))
			return;
		log.debug(className);
		log.debug(format(format, arg));
	}

	@Override
	public void debug(String format, Object arg1, Object arg2) {
		if (log.denyClassName(className))
			return;
		log.debug(className);
		log.debug(format(format, arg1, arg2));
	}

	@Override
	public void debug(String format, Object... arguments) {
		if (log.denyClassName(className))
			return;
		log.debug(className);
		log.debug(format(format, arguments));
	}

	@Override
	public void debug(String msg, Throwable throwable) {
		if (log.denyClassName(className))
			return;
		log.debug(className);
		log.debug(msg, throwable);
	}

	@Override
	public boolean isInfoEnabled() {
		return log.isInfoEnabled();
	}

	@Override
	public void info(String msg) {
		if (log.denyClassName(className))
			return;
		log.info(className);
		log.info(msg);
	}

	@Override
	public void info(String format, Object arg) {
		if (log.denyClassName(className))
			return;
		log.info(className);
		log.info(format(format, arg));
	}

	@Override
	public void info(String format, Object arg1, Object arg2) {
		if (log.denyClassName(className))
			return;
		log.info(className);
		log.info(format(format, arg1, arg2));
	}

	@Override
	public void info(String format, Object... arguments) {
		if (log.denyClassName(className))
			return;
		log.info(className);
		log.info(format(format, arguments));
	}

	@Override
	public void info(String format, Throwable throwable) {
		if (log.denyClassName(className))
			return;
		log.info(className);
		log.info(null, throwable);
	}

	@Override
	public boolean isWarnEnabled() {
		return log.isWarnEnabled();
	}

	@Override
	public void warn(String msg) {
		log.warn(msg);
	}

	@Override
	public void warn(String format, Object arg) {
		log.warn(format(format, arg));
	}

	@Override
	public void warn(String format, Object arg1, Object arg2) {
		log.warn(format(format, arg1, arg2));
	}

	@Override
	public void warn(String format, Object... arguments) {
		log.warn(format(format, arguments));
	}

	@Override
	public void warn(String format, Throwable throwable) {
		log.warn(null, throwable);
	}

	@Override
	public boolean isErrorEnabled() {
		return log.isErrorEnabled();
	}

	@Override
	public void error(String msg) {
		log.error(msg);
	}

	@Override
	public void error(String format, Object arg) {
		log.error(format(format, arg));
	}

	@Override
	public void error(String format, Object arg1, Object arg2) {
		log.error(format(format, arg1, arg2));
	}

	@Override
	public void error(String format, Object... arguments) {
		log.error(format(format, arguments));
	}

	@Override
	public void error(String msg, Throwable throwable) {
		log.error(msg, throwable);
	}

}
