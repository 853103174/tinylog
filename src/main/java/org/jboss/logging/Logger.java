package org.jboss.logging;

import static java.security.AccessController.doPrivileged;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.PrivilegedAction;
import java.util.Locale;

import log.tiny.TinyLog;
import log.util.CommUtil;
import log.util.Constant;

public abstract class Logger implements Serializable, BasicLogger {

	private static final long serialVersionUID = 4232175575988879434L;
	private static final TinyLog log = TinyLog.getInstance();
	private String className;

	public enum Level {
		FATAL(6), ERROR(5), WARN(4), INFO(3), DEBUG(2), TRACE(1);

		private int value;

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}

		private Level(int value) {
			this.value = value;
		}
	}

	protected Logger(final String name) {
		this.className = name;
	}

	public String getName() {
		return className;
	}

	protected void doLog(Level level, String loggerClassName, Object msg, Object[] parameters, Throwable throwable) {
		if (log.denyClassName(className))
			return;
		if (level.getValue() == Constant.TRACE || level.getValue() == Constant.DEBUG
				|| level.getValue() == Constant.INFO)
			log.writeLog(level.getValue(), className);
		StringBuilder infos = new StringBuilder();
		if (msg != null) {
			infos.append(msg);
		}
		if (parameters != null) {
			infos.append(TinyLog.endStr).append(parameters.toString());
		}
		if (throwable != null) {
			infos.append(TinyLog.endStr).append(CommUtil.getExpStack(throwable));
		}

		log.writeLog(level.getValue(), infos);
	}

	protected void doLogf(Level level, String loggerClassName, String format, Object[] parameters, Throwable thrown) {
		doLog(level, null, String.format(format, parameters), null, thrown);
	}

	@Override
	public boolean isEnabled(Level level) {
		return Constant.LOG_LEVEL <= level.getValue();
	}

	@Override
	public boolean isTraceEnabled() {
		return isEnabled(Level.TRACE);
	}

	@Override
	public void trace(Object msg) {
		doLog(Level.TRACE, null, msg, null, null);
	}

	@Override
	public void trace(Object msg, Throwable throwable) {
		doLog(Level.TRACE, null, msg, null, throwable);
	}

	@Override
	public void trace(String loggerFqcn, Object msg, Throwable throwable) {
		doLog(Level.TRACE, loggerFqcn, msg, null, throwable);
	}

	@Deprecated
	public void trace(Object msg, Object[] params) {
		doLog(Level.TRACE, null, msg, params, null);
	}

	@Deprecated
	public void trace(Object msg, Object[] params, Throwable throwable) {
		doLog(Level.TRACE, null, msg, params, throwable);
	}

	@Override
	public void trace(String loggerFqcn, Object msg, Object[] params, Throwable throwable) {
		doLog(Level.TRACE, loggerFqcn, msg, params, throwable);
	}

	@Override
	public void tracev(String format, Object... params) {
		doLog(Level.TRACE, null, format, params, null);
	}

	@Override
	public void tracev(String format, Object param1) {
		if (isEnabled(Level.TRACE)) {
			doLog(Level.TRACE, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void tracev(String format, Object param1, Object param2) {
		if (isEnabled(Level.TRACE)) {
			doLog(Level.TRACE, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void tracev(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.TRACE)) {
			doLog(Level.TRACE, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void tracev(Throwable throwable, String format, Object... params) {
		doLog(Level.TRACE, null, format, params, throwable);
	}

	@Override
	public void tracev(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.TRACE)) {
			doLog(Level.TRACE, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void tracev(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.TRACE)) {
			doLog(Level.TRACE, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void tracev(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.TRACE)) {
			doLog(Level.TRACE, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void tracef(String format, Object... params) {
		doLogf(Level.TRACE, null, format, params, null);
	}

	@Override
	public void tracef(String format, Object param1) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void tracef(String format, Object param1, Object param2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void tracef(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void tracef(Throwable throwable, String format, Object... params) {
		doLogf(Level.TRACE, null, format, params, throwable);
	}

	@Override
	public void tracef(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void tracef(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void tracef(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void tracef(final String format, final int arg) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg }, null);
		}
	}

	@Override
	public void tracef(final String format, final int arg1, final int arg2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2 }, null);
		}
	}

	@Override
	public void tracef(final String format, final int arg1, final Object arg2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2 }, null);
		}
	}

	@Override
	public void tracef(final String format, final int arg1, final int arg2, final int arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void tracef(final String format, final int arg1, final int arg2, final Object arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void tracef(final String format, final int arg1, final Object arg2, final Object arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final int arg) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final int arg1, final int arg2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2 }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final int arg1, final Object arg2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2 }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final int arg1, final int arg2, final int arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final int arg1, final int arg2,
			final Object arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final int arg1, final Object arg2,
			final Object arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void tracef(final String format, final long arg) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg }, null);
		}
	}

	@Override
	public void tracef(final String format, final long arg1, final long arg2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2 }, null);
		}
	}

	@Override
	public void tracef(final String format, final long arg1, final Object arg2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2 }, null);
		}
	}

	@Override
	public void tracef(final String format, final long arg1, final long arg2, final long arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void tracef(final String format, final long arg1, final long arg2, final Object arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void tracef(final String format, final long arg1, final Object arg2, final Object arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final long arg) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final long arg1, final long arg2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2 }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final long arg1, final Object arg2) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2 }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final long arg1, final long arg2,
			final long arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final long arg1, final long arg2,
			final Object arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void tracef(final Throwable throwable, final String format, final long arg1, final Object arg2,
			final Object arg3) {
		if (isEnabled(Level.TRACE)) {
			doLogf(Level.TRACE, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public boolean isDebugEnabled() {
		return isEnabled(Level.DEBUG);
	}

	@Override
	public void debug(Object msg) {
		doLog(Level.DEBUG, null, msg, null, null);
	}

	@Override
	public void debug(Object msg, Throwable throwable) {
		doLog(Level.DEBUG, null, msg, null, throwable);
	}

	@Override
	public void debug(String loggerFqcn, Object msg, Throwable throwable) {
		doLog(Level.DEBUG, loggerFqcn, msg, null, throwable);
	}

	@Deprecated
	public void debug(Object msg, Object[] params) {
		doLog(Level.DEBUG, null, msg, params, null);
	}

	@Deprecated
	public void debug(Object msg, Object[] params, Throwable throwable) {
		doLog(Level.DEBUG, null, msg, params, throwable);
	}

	@Override
	public void debug(String loggerFqcn, Object msg, Object[] params, Throwable throwable) {
		doLog(Level.DEBUG, loggerFqcn, msg, params, throwable);
	}

	@Override
	public void debugv(String format, Object... params) {
		doLog(Level.DEBUG, null, format, params, null);
	}

	@Override
	public void debugv(String format, Object param1) {
		if (isEnabled(Level.DEBUG)) {
			doLog(Level.DEBUG, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void debugv(String format, Object param1, Object param2) {
		if (isEnabled(Level.DEBUG)) {
			doLog(Level.DEBUG, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void debugv(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.DEBUG)) {
			doLog(Level.DEBUG, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void debugv(Throwable throwable, String format, Object... params) {
		doLog(Level.DEBUG, null, format, params, throwable);
	}

	@Override
	public void debugv(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.DEBUG)) {
			doLog(Level.DEBUG, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void debugv(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.DEBUG)) {
			doLog(Level.DEBUG, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void debugv(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.DEBUG)) {
			doLog(Level.DEBUG, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void debugf(String format, Object... params) {
		doLogf(Level.DEBUG, null, format, params, null);
	}

	@Override
	public void debugf(String format, Object param1) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void debugf(String format, Object param1, Object param2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void debugf(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void debugf(Throwable throwable, String format, Object... params) {
		doLogf(Level.DEBUG, null, format, params, throwable);
	}

	@Override
	public void debugf(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void debugf(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void debugf(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void debugf(final String format, final int arg) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg }, null);
		}
	}

	@Override
	public void debugf(final String format, final int arg1, final int arg2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2 }, null);
		}
	}

	@Override
	public void debugf(final String format, final int arg1, final Object arg2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2 }, null);
		}
	}

	@Override
	public void debugf(final String format, final int arg1, final int arg2, final int arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void debugf(final String format, final int arg1, final int arg2, final Object arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void debugf(final String format, final int arg1, final Object arg2, final Object arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final int arg) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final int arg1, final int arg2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2 }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final int arg1, final Object arg2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2 }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final int arg1, final int arg2, final int arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final int arg1, final int arg2,
			final Object arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final int arg1, final Object arg2,
			final Object arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void debugf(final String format, final long arg) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg }, null);
		}
	}

	@Override
	public void debugf(final String format, final long arg1, final long arg2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2 }, null);
		}
	}

	@Override
	public void debugf(final String format, final long arg1, final Object arg2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2 }, null);
		}
	}

	@Override
	public void debugf(final String format, final long arg1, final long arg2, final long arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void debugf(final String format, final long arg1, final long arg2, final Object arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void debugf(final String format, final long arg1, final Object arg2, final Object arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, null);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final long arg) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final long arg1, final long arg2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2 }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final long arg1, final Object arg2) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2 }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final long arg1, final long arg2,
			final long arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final long arg1, final long arg2,
			final Object arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public void debugf(final Throwable throwable, final String format, final long arg1, final Object arg2,
			final Object arg3) {
		if (isEnabled(Level.DEBUG)) {
			doLogf(Level.DEBUG, null, format, new Object[] { arg1, arg2, arg3 }, throwable);
		}
	}

	@Override
	public boolean isInfoEnabled() {
		return isEnabled(Level.INFO);
	}

	@Override
	public void info(Object msg) {
		doLog(Level.INFO, null, msg, null, null);
	}

	@Override
	public void info(Object msg, Throwable throwable) {
		doLog(Level.INFO, null, msg, null, throwable);
	}

	@Override
	public void info(String loggerFqcn, Object msg, Throwable throwable) {
		doLog(Level.INFO, loggerFqcn, msg, null, throwable);
	}

	@Deprecated
	public void info(Object msg, Object[] params) {
		doLog(Level.INFO, null, msg, params, null);
	}

	@Deprecated
	public void info(Object msg, Object[] params, Throwable throwable) {
		doLog(Level.INFO, null, msg, params, throwable);
	}

	@Override
	public void info(String loggerFqcn, Object msg, Object[] params, Throwable throwable) {
		doLog(Level.INFO, loggerFqcn, msg, params, throwable);
	}

	@Override
	public void infov(String format, Object... params) {
		doLog(Level.INFO, null, format, params, null);
	}

	@Override
	public void infov(String format, Object param1) {
		if (isEnabled(Level.INFO)) {
			doLog(Level.INFO, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void infov(String format, Object param1, Object param2) {
		if (isEnabled(Level.INFO)) {
			doLog(Level.INFO, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void infov(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.INFO)) {
			doLog(Level.INFO, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void infov(Throwable throwable, String format, Object... params) {
		doLog(Level.INFO, null, format, params, throwable);
	}

	@Override
	public void infov(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.INFO)) {
			doLog(Level.INFO, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void infov(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.INFO)) {
			doLog(Level.INFO, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void infov(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.INFO)) {
			doLog(Level.INFO, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void infof(String format, Object... params) {
		doLogf(Level.INFO, null, format, params, null);
	}

	@Override
	public void infof(String format, Object param1) {
		if (isEnabled(Level.INFO)) {
			doLogf(Level.INFO, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void infof(String format, Object param1, Object param2) {
		if (isEnabled(Level.INFO)) {
			doLogf(Level.INFO, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void infof(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.INFO)) {
			doLogf(Level.INFO, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void infof(Throwable throwable, String format, Object... params) {
		doLogf(Level.INFO, null, format, params, throwable);
	}

	@Override
	public void infof(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.INFO)) {
			doLogf(Level.INFO, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void infof(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.INFO)) {
			doLogf(Level.INFO, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void infof(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.INFO)) {
			doLogf(Level.INFO, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void warn(Object msg) {
		doLog(Level.WARN, null, msg, null, null);
	}

	@Override
	public void warn(Object msg, Throwable throwable) {
		doLog(Level.WARN, null, msg, null, throwable);
	}

	@Override
	public void warn(String loggerFqcn, Object msg, Throwable throwable) {
		doLog(Level.WARN, loggerFqcn, msg, null, throwable);
	}

	@Deprecated
	public void warn(Object msg, Object[] params) {
		doLog(Level.WARN, null, msg, params, null);
	}

	@Deprecated
	public void warn(Object msg, Object[] params, Throwable throwable) {
		doLog(Level.WARN, null, msg, params, throwable);
	}

	@Override
	public void warn(String loggerFqcn, Object msg, Object[] params, Throwable throwable) {
		doLog(Level.WARN, loggerFqcn, msg, params, throwable);
	}

	@Override
	public void warnv(String format, Object... params) {
		doLog(Level.WARN, null, format, params, null);
	}

	@Override
	public void warnv(String format, Object param1) {
		if (isEnabled(Level.WARN)) {
			doLog(Level.WARN, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void warnv(String format, Object param1, Object param2) {
		if (isEnabled(Level.WARN)) {
			doLog(Level.WARN, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void warnv(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.WARN)) {
			doLog(Level.WARN, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void warnv(Throwable throwable, String format, Object... params) {
		doLog(Level.WARN, null, format, params, throwable);
	}

	@Override
	public void warnv(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.WARN)) {
			doLog(Level.WARN, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void warnv(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.WARN)) {
			doLog(Level.WARN, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void warnv(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.WARN)) {
			doLog(Level.WARN, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void warnf(String format, Object... params) {
		doLogf(Level.WARN, null, format, params, null);
	}

	@Override
	public void warnf(String format, Object param1) {
		if (isEnabled(Level.WARN)) {
			doLogf(Level.WARN, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void warnf(String format, Object param1, Object param2) {
		if (isEnabled(Level.WARN)) {
			doLogf(Level.WARN, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void warnf(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.WARN)) {
			doLogf(Level.WARN, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void warnf(Throwable throwable, String format, Object... params) {
		doLogf(Level.WARN, null, format, params, throwable);
	}

	@Override
	public void warnf(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.WARN)) {
			doLogf(Level.WARN, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void warnf(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.WARN)) {
			doLogf(Level.WARN, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void warnf(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.WARN)) {
			doLogf(Level.WARN, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void error(Object msg) {
		doLog(Level.ERROR, null, msg, null, null);
	}

	@Override
	public void error(Object msg, Throwable t) {
		doLog(Level.ERROR, null, msg, null, t);
	}

	@Override
	public void error(String loggerFqcn, Object msg, Throwable throwable) {
		doLog(Level.ERROR, loggerFqcn, msg, null, throwable);
	}

	@Deprecated
	public void error(Object msg, Object[] params) {
		doLog(Level.ERROR, null, msg, params, null);
	}

	@Deprecated
	public void error(Object msg, Object[] params, Throwable throwable) {
		doLog(Level.ERROR, null, msg, params, throwable);
	}

	@Override
	public void error(String loggerFqcn, Object msg, Object[] params, Throwable throwable) {
		doLog(Level.ERROR, loggerFqcn, msg, params, throwable);
	}

	@Override
	public void errorv(String format, Object... params) {
		doLog(Level.ERROR, null, format, params, null);
	}

	@Override
	public void errorv(String format, Object param1) {
		if (isEnabled(Level.ERROR)) {
			doLog(Level.ERROR, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void errorv(String format, Object param1, Object param2) {
		if (isEnabled(Level.ERROR)) {
			doLog(Level.ERROR, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void errorv(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.ERROR)) {
			doLog(Level.ERROR, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void errorv(Throwable throwable, String format, Object... params) {
		doLog(Level.ERROR, null, format, params, throwable);
	}

	@Override
	public void errorv(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.ERROR)) {
			doLog(Level.ERROR, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void errorv(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.ERROR)) {
			doLog(Level.ERROR, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void errorv(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.ERROR)) {
			doLog(Level.ERROR, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void errorf(String format, Object... params) {
		doLogf(Level.ERROR, null, format, params, null);
	}

	@Override
	public void errorf(String format, Object param1) {
		if (isEnabled(Level.ERROR)) {
			doLogf(Level.ERROR, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void errorf(String format, Object param1, Object param2) {
		if (isEnabled(Level.ERROR)) {
			doLogf(Level.ERROR, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void errorf(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.ERROR)) {
			doLogf(Level.ERROR, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void errorf(Throwable throwable, String format, Object... params) {
		doLogf(Level.ERROR, null, format, params, throwable);
	}

	@Override
	public void errorf(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.ERROR)) {
			doLogf(Level.ERROR, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void errorf(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.ERROR)) {
			doLogf(Level.ERROR, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void errorf(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.ERROR)) {
			doLogf(Level.ERROR, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void fatal(Object msg) {
		doLog(Level.FATAL, null, msg, null, null);
	}

	@Override
	public void fatal(Object msg, Throwable throwable) {
		doLog(Level.FATAL, null, msg, null, throwable);
	}

	@Override
	public void fatal(String loggerFqcn, Object msg, Throwable throwable) {
		doLog(Level.FATAL, loggerFqcn, msg, null, throwable);
	}

	@Deprecated
	public void fatal(Object msg, Object[] params) {
		doLog(Level.FATAL, null, msg, params, null);
	}

	@Deprecated
	public void fatal(Object msg, Object[] params, Throwable t) {
		doLog(Level.FATAL, null, msg, params, t);
	}

	@Override
	public void fatal(String loggerFqcn, Object msg, Object[] params, Throwable throwable) {
		doLog(Level.FATAL, loggerFqcn, msg, params, throwable);
	}

	@Override
	public void fatalv(String format, Object... params) {
		doLog(Level.FATAL, null, format, params, null);
	}

	@Override
	public void fatalv(String format, Object param1) {
		if (isEnabled(Level.FATAL)) {
			doLog(Level.FATAL, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void fatalv(String format, Object param1, Object param2) {
		if (isEnabled(Level.FATAL)) {
			doLog(Level.FATAL, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void fatalv(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.FATAL)) {
			doLog(Level.FATAL, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void fatalv(Throwable throwable, String format, Object... params) {
		doLog(Level.FATAL, null, format, params, throwable);
	}

	@Override
	public void fatalv(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.FATAL)) {
			doLog(Level.FATAL, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void fatalv(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.FATAL)) {
			doLog(Level.FATAL, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void fatalv(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.FATAL)) {
			doLog(Level.FATAL, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void fatalf(String format, Object... params) {
		doLogf(Level.FATAL, null, format, params, null);
	}

	@Override
	public void fatalf(String format, Object param1) {
		if (isEnabled(Level.FATAL)) {
			doLogf(Level.FATAL, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void fatalf(String format, Object param1, Object param2) {
		if (isEnabled(Level.FATAL)) {
			doLogf(Level.FATAL, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void fatalf(String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.FATAL)) {
			doLogf(Level.FATAL, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void fatalf(Throwable throwable, String format, Object... params) {
		doLogf(Level.FATAL, null, format, params, throwable);
	}

	@Override
	public void fatalf(Throwable throwable, String format, Object param1) {
		if (isEnabled(Level.FATAL)) {
			doLogf(Level.FATAL, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void fatalf(Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(Level.FATAL)) {
			doLogf(Level.FATAL, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void fatalf(Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(Level.FATAL)) {
			doLogf(Level.FATAL, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void log(Level level, Object msg) {
		doLog(level, null, msg, null, null);
	}

	@Override
	public void log(Level level, Object msg, Throwable t) {
		doLog(level, null, msg, null, t);
	}

	@Override
	public void log(Level level, String loggerFqcn, Object msg, Throwable t) {
		doLog(level, loggerFqcn, msg, null, t);
	}

	@Deprecated
	public void log(Level level, Object msg, Object[] params) {
		doLog(level, null, msg, params, null);
	}

	@Deprecated
	public void log(Level level, Object msg, Object[] params, Throwable t) {
		doLog(level, null, msg, params, t);
	}

	@Override
	public void log(String loggerFqcn, Level level, Object msg, Object[] params, Throwable throwable) {
		doLog(level, loggerFqcn, msg, params, throwable);
	}

	@Override
	public void logv(Level level, String format, Object... params) {
		doLog(level, null, format, params, null);
	}

	@Override
	public void logv(Level level, String format, Object param1) {
		if (isEnabled(level)) {
			doLog(level, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void logv(Level level, String format, Object param1, Object param2) {
		if (isEnabled(level)) {
			doLog(level, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void logv(Level level, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(level)) {
			doLog(level, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void logv(Level level, Throwable throwable, String format, Object... params) {
		doLog(level, null, format, params, throwable);
	}

	@Override
	public void logv(Level level, Throwable throwable, String format, Object param1) {
		if (isEnabled(level)) {
			doLog(level, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void logv(Level level, Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(level)) {
			doLog(level, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void logv(Level level, Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(level)) {
			doLog(level, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void logv(String loggerFqcn, Level level, Throwable throwable, String format, Object... params) {
		doLog(level, loggerFqcn, format, params, throwable);
	}

	@Override
	public void logv(String loggerFqcn, Level level, Throwable throwable, String format, Object param1) {
		if (isEnabled(level)) {
			doLog(level, loggerFqcn, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void logv(String loggerFqcn, Level level, Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(level)) {
			doLog(level, loggerFqcn, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void logv(String loggerFqcn, Level level, Throwable throwable, String format, Object param1, Object param2,
			Object param3) {
		if (isEnabled(level)) {
			doLog(level, loggerFqcn, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void logf(Level level, String format, Object... params) {
		doLogf(level, null, format, params, null);
	}

	@Override
	public void logf(Level level, String format, Object param1) {
		if (isEnabled(level)) {
			doLogf(level, null, format, new Object[] { param1 }, null);
		}
	}

	@Override
	public void logf(Level level, String format, Object param1, Object param2) {
		if (isEnabled(level)) {
			doLogf(level, null, format, new Object[] { param1, param2 }, null);
		}
	}

	@Override
	public void logf(Level level, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(level)) {
			doLogf(level, null, format, new Object[] { param1, param2, param3 }, null);
		}
	}

	@Override
	public void logf(Level level, Throwable throwable, String format, Object... params) {
		doLogf(level, null, format, params, throwable);
	}

	@Override
	public void logf(Level level, Throwable throwable, String format, Object param1) {
		if (isEnabled(level)) {
			doLogf(level, null, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void logf(Level level, Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(level)) {
			doLogf(level, null, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void logf(Level level, Throwable throwable, String format, Object param1, Object param2, Object param3) {
		if (isEnabled(level)) {
			doLogf(level, null, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void logf(String loggerFqcn, Level level, Throwable throwable, String format, Object param1) {
		if (isEnabled(level)) {
			doLogf(level, loggerFqcn, format, new Object[] { param1 }, throwable);
		}
	}

	@Override
	public void logf(String loggerFqcn, Level level, Throwable throwable, String format, Object param1, Object param2) {
		if (isEnabled(level)) {
			doLogf(level, loggerFqcn, format, new Object[] { param1, param2 }, throwable);
		}
	}

	@Override
	public void logf(String loggerFqcn, Level level, Throwable throwable, String format, Object param1, Object param2,
			Object param3) {
		if (isEnabled(level)) {
			doLogf(level, loggerFqcn, format, new Object[] { param1, param2, param3 }, throwable);
		}
	}

	@Override
	public void logf(String loggerFqcn, Level level, Throwable throwable, String format, Object... params) {
		doLogf(level, loggerFqcn, format, params, throwable);
	}

	protected final Object writeReplace() {
		return null;
	}

	public static Logger getLogger(String name) {
		return LoggerProviders.PROVIDER.getLogger(name);
	}

	public static Logger getLogger(String name, String suffix) {
		return getLogger(name == null || name.length() == 0 ? suffix : name + "." + suffix);
	}

	public static Logger getLogger(Class<?> clazz) {
		return getLogger(clazz.getName());
	}

	public static Logger getLogger(Class<?> clazz, String suffix) {
		return getLogger(clazz.getName(), suffix);
	}

	public static <T> T getMessageLogger(Class<T> type, String category) {
		return getMessageLogger(type, category, Locale.CHINA);
	}

	public static <T> T getMessageLogger(final Class<T> type, final String category, final Locale locale) {
		return doPrivileged(new PrivilegedAction<T>() {
			public T run() {
				String language = locale.getLanguage();
				String country = locale.getCountry();
				String variant = locale.getVariant();

				Class<? extends T> loggerClass = null;
				final ClassLoader classLoader = type.getClassLoader();
				final String typeName = type.getName();
				if (variant != null && variant.length() > 0)
					try {
						loggerClass = Class
								.forName(join(typeName, "$logger", language, country, variant), true, classLoader)
								.asSubclass(type);
					} catch (ClassNotFoundException e) {
					}
				if (loggerClass == null && country != null && country.length() > 0)
					try {
						loggerClass = Class
								.forName(join(typeName, "$logger", language, country, null), true, classLoader)
								.asSubclass(type);
					} catch (ClassNotFoundException e) {
					}
				if (loggerClass == null && language != null && language.length() > 0)
					try {
						loggerClass = Class.forName(join(typeName, "$logger", language, null, null), true, classLoader)
								.asSubclass(type);
					} catch (ClassNotFoundException e) {
					}
				if (loggerClass == null)
					try {
						loggerClass = Class.forName(join(typeName, "$logger", null, null, null), true, classLoader)
								.asSubclass(type);
					} catch (ClassNotFoundException e) {
						throw new IllegalArgumentException(
								"Invalid logger " + type + " (implementation not found in " + classLoader + ")");
					}
				final Constructor<? extends T> constructor;
				try {
					constructor = loggerClass.getConstructor(Logger.class);
				} catch (NoSuchMethodException e) {
					throw new IllegalArgumentException(
							"Logger implementation " + loggerClass + " has no matching constructor");
				}
				try {
					return constructor.newInstance(Logger.getLogger(category));
				} catch (InstantiationException e) {
					throw new IllegalArgumentException(
							"Logger implementation " + loggerClass + " could not be instantiated", e);
				} catch (IllegalAccessException e) {
					throw new IllegalArgumentException(
							"Logger implementation " + loggerClass + " could not be instantiated", e);
				} catch (InvocationTargetException e) {
					throw new IllegalArgumentException(
							"Logger implementation " + loggerClass + " could not be instantiated", e.getCause());
				}
			}
		});
	}

	private static String join(String interfaceName, String a, String b, String c, String d) {
		final StringBuilder build = new StringBuilder();
		build.append(interfaceName).append('_').append(a);
		if (b != null && b.length() > 0) {
			build.append('_');
			build.append(b);
		}
		if (c != null && c.length() > 0) {
			build.append('_');
			build.append(c);
		}
		if (d != null && d.length() > 0) {
			build.append('_');
			build.append(d);
		}

		return build.toString();
	}

}