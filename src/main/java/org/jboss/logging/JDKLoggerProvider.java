package org.jboss.logging;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class JDKLoggerProvider extends AbstractMdcLoggerProvider implements LoggerProvider {

	private static final ConcurrentMap<String, Logger> loggerMap = new ConcurrentHashMap<String, Logger>();

	public Logger getLogger(final String name) {
		Logger log4jLogger = loggerMap.get(name);
		if (log4jLogger == null) {
			Logger newInstance = new JDKLogger(name);
			Logger oldInstance = loggerMap.putIfAbsent(name, newInstance);
			return oldInstance == null ? newInstance : oldInstance;
		} else {
			return log4jLogger;
		}
	}

}
